# Playground - Typescript - Decorators

My playground to experiment with typescript decorators.

# How to run?

`npm run start`

# Sources of information and ideas:

1. https://www.typescriptlang.org/docs/handbook/decorators.html
2. https://codeburst.io/decorate-your-code-with-typescript-decorators-5be4a4ffecb4
3. https://nikgrozev.com/2018/09/18/diy-type-safe-aop-in-typescript/
4. This one does work https://codeburst.io/decorate-your-code-with-typescript-decorators-5be4a4ffecb4
5. This one does work https://stackoverflow.com/questions/47512544/typescript-decorate-async-function