
import {auditWrap, measure_execution_time, other_decorator, C} from "./decorators"

function greeter(person: string) {
    return "Hello, " + person;
}

let user = "Mr. Cheeseburger";

console.log(greeter(user))

const c = new C()
c.method()

auditWrap(greeter)("Dan")