
function measure_execution_time() {
    console.log("measure_execution_time(): entered")
    const t0: any = new Date().getTime();
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const t1: any = new Date().getTime();
        console.log(`measure_execution_time(): exited. Execution time: ${t1 - t0} ms`)
    }
}

function other_decorator() {
    console.log("other_decorator(): entered")
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        console.log("other_decorator(): exited")
    }
}

const txt = (o: any) => JSON.stringify(o, null, 2);

const auditWrap = function <F extends Function>(fn: F): F {
    const wrapper = async (...args: any[]) => {
      const stringArgs = args.map(txt).join(",\n"); // Textualize all arguements
  
      try {
        console.log(`Attempting to call function: "${fn.name}" with arguements: ${stringArgs}`);
        const result = fn(...args);
        console.log(`Call to function "${fn.name}" suceeded with result: ${txt(result)}`);
        return result;
      } catch (e) {
        console.log(`Call to function "${fn.name}" failed with message: ${e.message}, Details: \n${txt(e)}`);
        throw e;
      }
    };
    return wrapper as any;
  }

class C {
    @measure_execution_time()
    @other_decorator()
    method() {
        console.log("method")
    }
}

export {
    measure_execution_time,
    other_decorator,
    auditWrap,
    C
}